require 'wordlist'

### Defaults
confdir = ENV['HOME'] + '/settings/mailvisa'
scorefile = 'scores'

usage = 'USAGE: ' + $0 + ' [options]'

help = <<EOT
Valid options:

-c <path>	Look for files in <path> (default: $HOME/settings/mailvisa)
-f <path>	Load scores from <path> (default: scores)
EOT

## Process command line
i = 0
while i < ARGV.length
	case ARGV[i]
	when '-h'
		puts usage
		puts "\n" + help
		exit
	when '-c'
		i = i + 1
		confdir = ARGV[i]
	when '-f'
		i = i + 1
		scorefile = ARGV[i]
	when /^-/
		$stderr.puts 'Unknown option: ' + ARGV[i]
		$stderr.puts usage
		$stderr.puts 'Use "' + $0 + ' -h" for help'
		exit 0x80
	else
		scorefile = ARGV[i]
	end
	i = i + 1
end

scorefile = confdir + '/' + scorefile if scorefile.index('/') == nil

## Display scores
fh = open scorefile
db = load_wordlist fh
fh.close

db[:words].each do |word,score|
	puts "#{word}: #{score}"
end
