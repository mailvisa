ruby = ENV['RUBY']
packagedir = ENV['packagedir']

puts <<EOF
#! #{ruby}

#### Mailvisa wrapper

### Configuration

packagedir = #{packagedir.inspect}
$:.unshift packagedir

usage = "USAGE: \#{$0} <command> [options]"

help = "Valid commands are:

add		Add messages to database
calculate	Recalculate scores
check		Check whether a message is spam
remove		Remove messages from database
start		Start daemon
view		View scores

Use \#{$0} <command> -h to list options for that command"

### Main program

arg = ARGV.shift

case arg
when 'check'
	load 'mailvisa.rb'
when 'start'
	load 'mailvisad.rb'
when 'add'
	load 'add_messages.rb'
when 'remove'
	load 'remove_messages.rb'
when 'view'
	load 'view_scores.rb'
when 'calculate'
	load 'calculate_scores.rb'
when 'help'
	puts help
else
	$stderr.puts usage
	$stderr.puts help
	exit 0x80
end
EOF
