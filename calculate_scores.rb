require 'wordlist'

### Defaults

confdir = ENV['HOME'] + '/settings/mailvisa'
scorefile = 'scores'
goodfile = 'good'
badfile = 'bad'
$good_multiplier = 1

### Functions

def calculate_word_score word
	bad_count = $bad_words[word]
	good_count = $good_words[word]
	b = bad_count != nil ? ($bad_words[word].to_f / $bad_messages) : 0
	g = good_count != nil ?
		($good_multiplier * $good_words[word].to_f / $good_messages) :
		0
	s = b / (b + g)
	if s < 0.1
		0.1
	elsif s > 0.9
		0.9
	else
		s
	end
end

usage = 'USAGE: ' + $0 + ' [options]'

help = <<EOT
Valid options are:

-c <path>	Look for files in <path> (default: $HOME/settings/mailvisa)
-g <file>	Load good words from <file> (default: "good")
-b <file>	Load bad words from <file> (default: "bad")
-f <file>	Write scores to <file> (default: "scores")
-m <num>	Multiply number of good occurrences by <num> (default: 1.0)
EOT

### Main program

## Process command line
i, n  = 0, 0
while i < ARGV.length
	case ARGV[i]
	when '-c'
		i = i + 1
		confdir = ARGV[i]
	when '-g'
		i = i + 1
		goodfile = ARGV[i]
	when '-b'
		i =i + 1
		badfile = ARGV[i]
	when '-f'
		i = i + 1
		scorefile = ARGV[i]
	when '-h'
		puts usage
		print "\n" + help
		exit
	when '-m'
		i = i + 1
		$good_multiplier = ARGV[i].to_f
	when /^-/
		$stderr.puts 'Unknown option: ' + ARGV[i]
		$stderr.puts 'See "' + $0 + ' -h" for valid options'
		exit 0x80
	else
		## Compatibility with original version
		case n
		when 0
			goodfile = ARGV[i]
		when 1
			badfile = ARGV[i]
		when 2
			scorefile = ARGV[i]
		else
			raise 'Too many arguments'
		end
	end
	i = i + 1
end

goodfile = confdir + '/' + goodfile if goodfile.index('/') == nil
badfile = confdir + '/' + badfile if badfile.index('/') == nil
scorefile = confdir + '/' + scorefile if scorefile.index('/') == nil

print "Loading good words from #{goodfile}..."
$stdout.flush
good = load_wordlist open(goodfile)
$good_words = good[:words]
$good_messages = good[:messages]
puts "#{$good_words.length} words loaded"

print "Loading bad words from #{badfile}..."
$stdout.flush
bad = load_wordlist open(badfile)
$bad_words = bad[:words]
$bad_messages = bad[:messages]
puts "#{$bad_words.length} words loaded"

print "Calculating probabilities..."
$stdout.flush
score = {}
$bad_words.each do |word,count|
	if score[word] == nil && count > 4
		score[word] = calculate_word_score word
	end
end

$good_words.each do |word,count|
	if score[word] == nil && count > 4
		score[word] = calculate_word_score word
	end
end
puts 'done'

fh = open scorefile, 'w'
wordlist = { :messages => ($good_messages + $bad_messages), :words => score }
dump_wordlist wordlist, fh
fh.close
