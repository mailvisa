require 'wordlist'
require 'tokenize'

### Defaults
confdir = ENV['HOME'] + '/settings/mailvisa'
$ignorespamheaders = true
filename = nil
messages = []

### Functions
def remove_stream words, stream
	stream.each do |line|
		next if line[0,6] == 'X-Spam'
		tokenize(line).each do |word|
			next if word.length > 40
			count = words[word]
			case count
			when nil
				nil
			when 1
				words[word] = nil
			else
				words[word] = count - 1
			end
		end
	end
end

usage = 'USAGE: ' + $0 + ' [options] <wordlist> [<message> ...]'

help = <<EOT
Valid options are:

-c <path>	Look for configuration files in <path>
			(default: $HOME/settings/mailvisa)
-i		Include X-Spam headers in analysis
EOT

### Main program

## Process command line
i = 0
while i < ARGV.length
	case ARGV[i]
	when '-h'
		puts usage
		print "\n" + help
		exit
	when '-c'
		i = i + 1
		confdir = ARGV[i]
	when /^-/
		$stderr.puts 'Invalid option: ' + ARGV[i]
		$stderr.puts usage
		exit 0x80
	else
		filename = ARGV[i]
		i = i + 1
		messages = ARGV[i..-1]
		break
	end
	i = i + 1
end

if filename == nil
	$stderr.puts 'No wordlist specified'
	$stderr.puts usage
	exit 0x80
end

filename = confdir + '/' + filename if filename.index('/') == nil

begin
	fh = open filename
rescue
	print filename + ' not found, will create new file'
	fh = false
end

if fh
	$stderr.print "Loading #{filename}..."
	wordlist = load_wordlist fh
	fh.close
	words = wordlist[:words]
	message_count = wordlist[:messages]
	$stderr.puts words.length.to_s + ' words loaded'
else
	words = {}
	message_count = 0
end

if messages.length > 0
	messages.each do |x|
		$stderr.puts "Removing #{x}"
		fh = open x
		remove_stream words, fh
		fh.close
		message_count = message_count - 1
	end
else
	remove_stream words, $stdin
	message_count = message_count - 1
end


message_count = 0 if message_count < 0

wordlist = {
	:messages => message_count,
	:words => words
}

$stderr.print "Writing #{filename}..."
fh = open filename, 'w'
dump_wordlist wordlist, fh
fh.close
$stderr.puts 'done'
