def dump_wordlist wordlist, stream
	stream.puts 'Content-Type: text/x-mailvisa-wordlist'
	stream.puts 'Version: 1'
	stream.puts 'Messages: ' + wordlist[:messages].to_s
	stream.puts
	wordlist[:words].each { |k,v| stream.puts k + "\t" + v.to_s }
end

def load_wordlist stream
	header = {}
	while true
		line = stream.gets
		break if line == nil
		line = line.chomp
		break if line == ''
		key, value = line.split ': '
		header[key] = value
	end

	raise 'Invalid file format' if
		header['Content-Type'] != 'text/x-mailvisa-wordlist'
	raise 'Unsupported wordlist version: ' + header['Version'] if
		header['Version'] != '1'

	wordlist = {
		:messages => header['Messages'].to_i
	}

	words = {}
	stream.each do |line|
		key, value = line.split "\t"
		words[key] = value.to_f
	end

	wordlist[:words] = words
	wordlist
end
