package ?= mailvisa
version ?= 1.0.1

include Makefile.cfg
packagedir ?= $(datadir)/$(package)-$(version)
truepackagedir ?= $(truedatadir)/$(package)-$(version)

TARGETS ?= mailvisa

BINARIES ?= mailvisa
MANPAGES ?= mailvisa.1
FILES ?= add_messages.rb calculate_scores.rb \
	mailvisa.rb mailvisad.rb remove_messages.rb \
	view_scores.rb wordlist.rb tokenize.rb \
	$(BINARIES)

all: $(TARGETS)

mailvisa: make_mailvisa.rb Makefile.cfg
	env RUBY='$(RUBY)' packagedir='$(packagedir)' '$(RUBY)' \
		make_mailvisa.rb > $@
	chmod +x $@

clean:

distclean: clean
	-rm $(TARGETS)

install: install-bin install-man

install-bin: $(BINARIES)
	[ -d '$(packagedir)' ] || mkdir -p '$(packagedir)'
	[ -d '$(bindir)' ] || mkdir -p '$(bindir)'
	cp $(FILES) '$(packagedir)/'
	for i in $(BINARIES); do \
		ln -s '$(truepackagedir)'/"$$i" '$(bindir)/'; \
	done

install-man: $(MANPAGES)
	[ -d '$(packagedir)' ] || mkdir -p '$(packagedir)'
	[ -d '$(mandir)/man1' ] || mkdir -p '$(mandir)/man1'
	cp $(MANPAGES) '$(packagedir)/'
	for i in $(MANPAGES); do \
		ln -s '$(truepackagedir)'/"$$i" '$(mandir)/man1/'; \
	done

.PHONY: all clean distclean install install-bin install-man
